//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
var http = require('http');
var path = require('path');

var stamplay = require("stamplay");
var Stamplay = new stamplay("test-a", "76ef01ad02d92cf7d11bb9e0051917dba989871c2f382988adba2978c23ea1ce");


var async = require('async');
var socketio = require('socket.io');
var express = require('express');

//
// ## SimpleServer `SimpleServer(obj)`
//
// Creates a new instance of SimpleServer with the following options:
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
//
var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use(express.static(path.resolve(__dirname, 'client')));
var messages = [];
var sockets = [];


io.on('connection', function (socket) {
    messages.forEach(function (data) {
      socket.emit('message', data);
    });

    socket.emit('servidor', "https://node-nellrcs.c9users.io/");
    sockets.push(socket);

    socket.on('disconnect', function () {
      sockets.splice(sockets.indexOf(socket), 1);
      broadcast('message', new Date().toISOString());
      updateRoster();
    });

    socket.on('message', function (msg) {
      var text = String(msg || '');

      if (!text)
        return;

      socket.get('name', function (err, name) {
        var data = {
          name: name,
          text: msg,
          hora: new Date().toISOString()
        };

        broadcast('message', data);
        //messages.push(data);
      });
    });
  

    socket.on('identify', function (name) {
     Stamplay.Object("pessoa").patch(name,{}, function(err,res){
        if(res){
          res = JSON.parse(res);
          socket.set('name', {"nome":res.nome,"telefone":res.telefone,"hora":new Date().toISOString()}, function (err) {
            updateRoster();
          });
        }
      });
    });
  });

function updateRoster() {
  async.map(
    sockets,
    function (socket, callback) {
      socket.get('name', callback);
    },
    function (err, names) {
  
        
        var newNames = [];
        names.forEach(function(item) { 
         if(item) newNames.push(item);
        });
 
        broadcast('roster', newNames);
        
    }
  );
}

function broadcast(event, data) {
  
  sockets.forEach(function (socket) {
    //console.log(socket);
    socket.emit(event, data);
  });
}



server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});
