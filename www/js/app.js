// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ngCordova','ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.controller('cadastrar',function($scope,$http,$cordovaBarcodeScanner,$cordovaGeolocation,$interval){
  
  $scope.aguarde = false;
  $scope.user = false; 
  $scope.localServer = "http://localhost:3000";  

  $scope.latitude = null;
  $scope.logitude = null;

  if(window.localStorage['id']){
      $scope.user = window.localStorage['id']; 
  };

  $scope.localizacao = function(){
  var posOptions = {timeout: 1000, enableHighAccuracy: false};  
  $cordovaGeolocation.getCurrentPosition(posOptions)
    .then(function (position) {
      var lat  = position.coords.latitude
      var long = position.coords.longitude
      $scope.latitude = lat;
      $scope.logitude = long;
      
      if($scope.roster){
        console.log(lat);
        socket.emit('message', {"lat":lat,"log":long});

      }
    }, function(err) {
      // error
    });
  }

    $interval($scope.localizacao, 500);

  
  $scope.enviar = function(dadosEnv){
    var req = {
			method: 'POST',
			url: 'https://test-a.stamplayapp.com/api/cobject/v1/pessoa',
			data: dadosEnv,
      headers: { 'Content-Type': 'application/json' }
    };
    
    $scope.aguarde = "Aguarde...";
    $http(req).then(function successCallback(response) {
      if(response.status == "200" || response.status == "201" || response.status == "202"){
        window.localStorage['id'] = response.data.id;
        $scope.user = response.data.id; 
        $scope.dadosEnv = null;
        $scope.aguarde = false;
      }
    }, function errorCallback(response) {
      $scope.aguarde = false;
    }); 

    $scope.dadosEnv = null;
    
  }
  
  $scope.esquecer = function(){
    window.localStorage['id'] = "";
    $scope.user = false; 
  }
  
  $scope.lerQrcodeChave =function(){
    
      $cordovaBarcodeScanner.scan().then(function(imageData) { 
        $scope.localServer = imageData.text; 
        $scope.conectar(imageData.text);
      }, function(error) {
        
      });
  }
  
/////////////////////////////////////

      var socket = null;

      $scope.messages = [];
      $scope.roster = null;
      $scope.name = '';
      $scope.text = '';

      $scope.conectar = function(localServer){
          $scope.conectando = true;
          socket = io.connect(localServer);
          socket.on('connect', function () {
            socket.emit('identify', window.localStorage['id']);
          });

          socket.on('roster', function (names) {
            $scope.roster = names;
            $scope.conectando = false;
            $scope.$apply();
        });
      }



})
